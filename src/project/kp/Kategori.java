package project.kp;

import java.util.ArrayList;
import java.util.List;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Kategori extends ListActivity implements OnClickListener{
	
	private List<String> DataFilter = new ArrayList<String>();
	SQLiteDatabase myDB= null;
	String TableName = "tbFiltering";
	String Data ="";
	String kat="";
	String konten="";
	String kategorine="";
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bun = this.getIntent().getExtras();
		kat = bun.getString("type");
		if(getList(kat).isEmpty()){
			if (kat.equals("key")){
				Toast.makeText(getApplicationContext(), "Blocked Keyword is Empty", Toast.LENGTH_LONG).show();
			}else if(kat.equals("number")){
				Toast.makeText(getApplicationContext(), "Blocked PhoneNumber is Empty", Toast.LENGTH_LONG).show();
			}	
		}else{
			Toast.makeText(getApplicationContext(), ""+getList(kat).size()+" blocked "+kat+"", Toast.LENGTH_LONG).show();
			setListAdapter(new ArrayAdapter<String>(this, R.layout.viewlist, getList(kat)));
			ListView list = getListView();
			list.setTextFilterEnabled(true);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
						konten = (String) ((TextView) arg1).getText();
						onClick(arg1);
				}
			});	
		}
	}
	
	public List<String> getList(String kategori){
		try{
			myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
			Cursor c =myDB.rawQuery("SELECT * FROM "+TableName+" WHERE Kategori='"+kategori+"'", null);
			int Coloumn2= c.getColumnIndex("Isi");
			c.moveToLast();
			DataFilter.clear();
			if (c!=null){
				do {
					
					String Isi= c.getString(Coloumn2);
					Data = Data+Isi;
					DataFilter.add(Isi);
				}
				while (c.moveToPrevious());
			}
			
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		return DataFilter;
    }

	@Override
	public void onClick(View arg0) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setMessage("Delete this Keyword?");
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            	getListDel(konten);
            	if (kat.equals("key")){
            		Toast.makeText(getApplicationContext(), "Delete Blocked Keyword successful", Toast.LENGTH_SHORT).show();
            	}else if(kat.equals("number")){
            		Toast.makeText(getApplicationContext(), "Delete Blocked Phone successful", Toast.LENGTH_SHORT).show();
            	}
            	finish();
            	startActivity(getIntent());
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();		
	}
        
	public List<String> getListDel(String nilai){
		try{
			myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
			myDB.execSQL("DELETE FROM "+TableName+" WHERE Isi='"+nilai+"'");
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		
		return DataFilter;
    }
//TombolMenu
	
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater awesome = getMenuInflater();
		awesome.inflate(R.menu.kategori_block, menu);
		return true;
	}
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.dellall:
			try{
				myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
				myDB.execSQL("DELETE FROM "+TableName+" WHERE Kategori='"+kat+"'");
				finish();
				startActivity(getIntent());
			}
			catch (Exception e){
				Log.e("Error", "Error",e);
			}
			finally{
				if (myDB!=null)
					myDB.close();
			}
			return false;
		
		}
		return false;
	}
//Tombol Back
    public void onBackItemSelected(){
    	startActivity(new Intent("project.kp.SETTING"));
    }
}
