package project.kp;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import android.net.Uri;

public class SmsReceiver extends BroadcastReceiver {

	private List<String> DataFilter = new ArrayList<String>();
	private List<String> DataFilterNumber = new ArrayList<String>();
	
	SQLiteDatabase myDB= null;
	String TableName = "tbFiltering";
	String TableFiltering = "tbPesanFilter";
	String TableActivation = "tbActivation";
	
	String kat="";
	Context kontek;
	boolean status=true;
	String NomerPengirim = "";
	String SMSSave="";
	String KategoriNilai="";
	String Value="";
	
    private SmsMessage[] msgs;
    
    @Override
    public void onReceive(Context context, Intent intent) 
    {
    	
    	
    	kontek = context;
        Bundle bundle = intent.getExtras();        
        msgs = null;
        String str = "";            

        if (bundle != null)
        {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];            
            for (int i=0; i<msgs.length; i++)
            {
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
                str += "SMS from " + msgs[i].getOriginatingAddress();                     
                str += ":";
                str += msgs[i].getMessageBody().toString();
                str += "\n";
                NomerPengirim = msgs[i].getOriginatingAddress(); 
                SMSSave = msgs[i].getMessageBody().toString();
            }
            
            if(getcodeActivation("key")==1){
		            getList("key");
		            int k=0;
		            while (k<DataFilter.size()){
			            if (str.toLowerCase().contains(DataFilter.get(k))){
			            	try {
				            	Thread.sleep(2000);
				            	KategoriNilai = "key";
				            	Value = DataFilter.get(k);
				            	Uri uriSms = Uri.parse("content://sms/inbox");
				            	Cursor c = context.getContentResolver().query(uriSms,new String[] { "_id", "thread_id" }, null, null, null);
				            	if (c != null && c.moveToPosition(0)) {
					            		String threadId = c.getString(0);
					            			context.getContentResolver().delete(Uri.parse("content://sms/" + threadId),null, null);
				            	}
				            }catch (Exception e) {
				            		System.out.println("Exception:: "+e);
				            }    
				            status=false;
				            SaveFilter(NomerPengirim, SMSSave, KategoriNilai, Value);
				            break;
			            }
			            k++;
		            }
		            DataFilter.clear();
        	}
            
            if(getcodeActivation("phone")==1){
		        	getList("number");
		            int j=0;
		            while (j<DataFilterNumber.size()){
			            if (DataFilterNumber.get(j).equals(NomerPengirim)){
			            	try {
				            	Thread.sleep(2000);
				            	KategoriNilai = "number";
				            	Value = DataFilterNumber.get(j);
				            	Uri uriSms = Uri.parse("content://sms/inbox");
				            	Cursor c = context.getContentResolver().query(uriSms,new String[] { "_id", "thread_id" }, null, null, null);
				            	if (c != null && c.moveToPosition(0)) {
					            		String threadId = c.getString(0);
					            			context.getContentResolver().delete(Uri.parse("content://sms/" + threadId),null, null);
				            	}
				            }catch (Exception e) {
				            		System.out.println("Exception:: "+e);
				            }    
				            status=false;
				            SaveFilter(NomerPengirim, SMSSave, KategoriNilai, Value);
				            break;
					            
			            }
			            j++;
		            }
		            DataFilterNumber.clear();
        	}
            
            if (status==true){
        		Toast.makeText(context, str, Toast.LENGTH_LONG).show();
            }else{
            	Toast.makeText(context, "New message has been blocked", Toast.LENGTH_LONG).show();
            	status=true;
            }            
        }
    }
    
    
    public int getcodeActivation(String kategori_activation){
    	int temp = 0; 
    	try{
			myDB = kontek.openOrCreateDatabase("SMSFiltering",0, null);
			Cursor c =myDB.rawQuery("SELECT "+kategori_activation+" FROM "+TableActivation+"" , null);
			int column = c.getColumnIndex(kategori_activation);
			c.moveToLast();
			if (c.getString(column).equals("true")){
				temp = 1;
			}else{
				temp = 0;
			}
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
    	return temp;    	
    }
    
    public List<String> getList(String kategori){
    	try{
			myDB = kontek.openOrCreateDatabase("SMSFiltering",0, null);
			Cursor c =myDB.rawQuery("SELECT * FROM "+TableName+" WHERE Kategori='"+kategori+"'", null);
			int Coloumn2= c.getColumnIndex("Isi");
			c.moveToLast();
			DataFilter.clear();
			if (c!=null){
				do {
					String Isi= c.getString(Coloumn2);
					if (kategori.equals("key")){
						DataFilter.add(Isi);
					}else if(kategori.equals("number")){
						DataFilterNumber.add(Isi);
					}
				}
				while (c.moveToPrevious());
			}
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		return DataFilter;
    }
    
    public void SaveFilter (String phone,String pesan,String kategori,String nilai){
    	try{
			myDB = kontek.openOrCreateDatabase("SMSFiltering",0, null);
			myDB.execSQL("CREATE TABLE IF NOT EXISTS "+TableFiltering+"(idSMS VARCHAR, phone VARCHAR, pesan VARCHAR, kategori VARCHAR, nilai VARCHAR);");
			Cursor c =myDB.rawQuery("SELECT idSMS FROM "+TableFiltering+"", null);
			if (c.getCount()==0){
				myDB.execSQL("INSERT INTO "+TableFiltering+"(idSMS,phone,pesan,kategori,nilai)"+" VALUES('1','"+phone+"','"+pesan+"','"+kategori+"','"+nilai+"');");
			}else{
				c.moveToLast();
				int temp = Integer.parseInt(c.getString(0))+1;
				
				myDB.execSQL("INSERT INTO "+TableFiltering+"(idSMS,phone,pesan,kategori,nilai)"+" VALUES('"+temp+"','"+phone+"','"+pesan+"','"+kategori+"','"+nilai+"');");
			}
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
    }

}