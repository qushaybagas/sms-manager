package project.kp;

import java.util.ArrayList;
import java.util.List;


import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FilteredSMS extends ListActivity implements OnClickListener{
	private List<String> DataFilter = new ArrayList<String>();
	SQLiteDatabase myDB= null;
	String TableName = "tbPesanFilter";
	String Data,DataShow ="";
	String kat="";
	public AlertDialog myAlertDialog;
	String[] rowcontent,content1,content2,content3,content4;
	String id;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(getList().isEmpty()){
				Toast.makeText(getApplicationContext(), "Blocked Message is Empty", Toast.LENGTH_LONG).show();
		}else{
			setListAdapter(new ArrayAdapter<String>(this, R.layout.filtersms, getList()));
			ListView list = getListView();
			list.setTextFilterEnabled(true);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
					String isi = (String) ((TextView) arg1).getText();
					rowcontent = isi.split("\n");
					content1 = rowcontent[0].split(":");
					content2 = rowcontent[1].split(":");
					content3 = rowcontent[2].split(":");
					content4 = rowcontent[3].split(":");		
					id = content4[1].trim();					
					onClick(arg1);
				}
			});			
		}
	}
	
	public List<String> getList(){
		try{
			myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
			Cursor c =myDB.rawQuery("SELECT * FROM "+TableName+" ", null);
			int Coloumn1= c.getColumnIndex("phone");
			int Coloumn2= c.getColumnIndex("idSMS");
			int Coloumn3= c.getColumnIndex("kategori");
			int Coloumn4= c.getColumnIndex("nilai");
			
			c.moveToFirst();
			DataFilter.clear();
			if (c!=null){
				do {
					String phone= c.getString(Coloumn1);
					String id= c.getString(Coloumn2);
					String gori= c.getString(Coloumn3);
					String nilai= c.getString(Coloumn4);
					Data = "Phone	: "+phone+"\nCategory : "+gori;
					DataShow = "Phone	: "+phone+"\nCategory : "+gori+"\nValue		: "+nilai+"\nId			: "+id; 
					DataFilter.add(DataShow);
				}
				while (c.moveToNext());
			}
			
			
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		
		return DataFilter;
    }
   	  
    @Override
	public void onClick(View arg0) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setMessage("Message : \n"+getmessage());
		alertbox.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            	Delmessage();
            	Toast.makeText(getApplicationContext(), "Delete Message successful", Toast.LENGTH_SHORT).show();
            	finish();
            	startActivity(getIntent());
            }
        });
        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();	    	
	}
	
    public void Delmessage(){
		try{
			myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
			myDB.execSQL("DELETE FROM "+TableName+" WHERE idSMS="+id);
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
    }
    
	public String getmessage(){
		String temp = "";
		try{
			myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
			Cursor c =myDB.rawQuery("SELECT pesan FROM "+TableName+" WHERE idSMS="+id+" ", null);
			int Coloumn1= c.getColumnIndex("pesan");
			c.moveToFirst();
			temp = c.getString(Coloumn1);
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		
	return temp;
    }
	
	
// Tombol Menu
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater awesome = getMenuInflater();
		awesome.inflate(R.menu.pesan_terblock, menu);
		return true;
	}
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.dellall:
			try{
				myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
				myDB.execSQL("DELETE FROM "+TableName+"");
				finish();
				startActivity(getIntent());
			}
			catch (Exception e){
				Log.e("Error", "Error",e);
			}
			finally{
				if (myDB!=null)
					myDB.close();
			}
			return false;
		
		case R.id.dellkey:
			try{
				myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
				myDB.execSQL("DELETE FROM "+TableName+" WHERE kategori='key'");
				finish();
				startActivity(getIntent());
			}
			catch (Exception e){
				Log.e("Error", "Error",e);
			}
			finally{
				if (myDB!=null)
					myDB.close();
			}
			return false;
			
		case R.id.dellnumber:
			try{
				myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
				myDB.execSQL("DELETE FROM "+TableName+" WHERE kategori='number'");
				finish();
				startActivity(getIntent());
			}
			catch (Exception e){
				Log.e("Error", "Error",e);
			}
			finally{
				if (myDB!=null)
					myDB.close();
			}
			return false;
		

		}
		return false;
	}
 //TOmbol Back
    public void onBackItemSelected(){
    	startActivity(new Intent("project.kp.UTAMA"));
    }
}
