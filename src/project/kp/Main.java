package project.kp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class Main extends Activity{
	CheckBox keyActive,phoneActive;
	SQLiteDatabase myDB= null;
	String TableName = "tbActivation";
	String TableFiltering = "tbFiltering";
	String TablePesanFilter = "tbPesanFilter";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        keyActive = (CheckBox) findViewById(R.id.check_key);
        phoneActive = (CheckBox) findViewById(R.id.check_phone);
        
        try{
	    	myDB = openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
	    	myDB.execSQL("CREATE TABLE IF NOT EXISTS "+TableName+"(key VARCHAR, phone VARCHAR);");
	    	myDB.execSQL("CREATE TABLE IF NOT EXISTS "+TableFiltering+"(Kategori VARCHAR, Isi VARCHAR);");
	    	myDB.execSQL("CREATE TABLE IF NOT EXISTS "+TablePesanFilter+"(idSMS VARCHAR, phone VARCHAR, pesan VARCHAR, kategori VARCHAR, nilai VARCHAR);");
			Cursor c =myDB.rawQuery("SELECT * FROM "+TableName+"", null);
	    	if (c.getCount()==0){
	    		myDB.execSQL("INSERT INTO "+TableName+"(key,phone) VALUES('false','false');");
			}
	    	Cursor cur =myDB.rawQuery("SELECT * FROM "+TableName+"", null);
	    	int Coloumn1= cur.getColumnIndex("key");
			int Coloumn2= cur.getColumnIndex("phone");
			
	    	cur.moveToLast();
	    	if (cur.getString(Coloumn1).equals("true")){
	    		keyActive.setChecked(true);
	    	}else{
	    		keyActive.setChecked(false);
	    	}
	    	
	    	if (cur.getString(Coloumn2).equals("true")){
	    		phoneActive.setChecked(true);
	    	}else{
	    		phoneActive.setChecked(false);
	    	}
    	}
    	catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
        
        keyActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton ButtonView, boolean isChecked) {
				if(isChecked){
					updateActivation("true", "key");
				}else{
					updateActivation("false", "key");
				}
			}
		});
        
        phoneActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton ButtonView, boolean isChecked) {
				if(isChecked){
					updateActivation("true", "phone");
				}else{
					updateActivation("false", "phone");
				}
			}
		});
    }
    
    public int updateActivation(String nilai,String kategori){
    	
    	try{
			myDB = this.openOrCreateDatabase("SMSFiltering",0, null);
			myDB.rawQuery("SELECT * FROM "+TableName+"" , null);
				if (kategori=="key"){
					myDB.execSQL("UPDATE "+TableName+" SET key='"+nilai+"'");
					if (nilai=="true"){
						Toast.makeText(getApplicationContext(), "Keyword Blocker is Active", Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(getApplicationContext(), "Keyword Blocker is Not Active", Toast.LENGTH_SHORT).show();
					}
				}else if (kategori=="phone"){
					myDB.execSQL("UPDATE "+TableName+" SET phone='"+nilai+"'");
					if (nilai=="true"){
						Toast.makeText(getApplicationContext(), "Phone Number Blocker is Active", Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(getApplicationContext(), "Phone Number Blocker is Not Active", Toast.LENGTH_SHORT).show();
					}
				}
	    		
						
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
    	
    	return 1;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater awesome = getMenuInflater();
		awesome.inflate(R.menu.main_menu, menu);
		return true;
	}
    
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menuMessage:
			startActivity(new Intent("project.kp.FILTER"));
			return false;
		case R.id.menuUtama:
			startActivity(new Intent("project.kp.UTAMA"));
			return false;
		case R.id.menuSetting:
			startActivity(new Intent("project.kp.SETTING"));
			return false;

		}
		return false;
	}

}