package project.kp;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
public class Setting extends Activity implements OnCheckedChangeListener{
	
	String Kategoridipilih="key";
	RadioButton TypeKeyword,TypeNumber;
	RadioGroup TypeUnit;
	Button btSave,btViewKey,btViewPhone;
    SQLiteDatabase myDB= null;
	String TableName = "tbFiltering";
	String Data ="";
	EditText InputDataKW,InputDataPN;
	List<String> DataFilter = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);

		TypeUnit = (RadioGroup) findViewById(R.id.rgType);
		TypeUnit.setOnCheckedChangeListener(this);
		TypeKeyword=(RadioButton)findViewById(R.id.rbKW);
		TypeNumber=(RadioButton)findViewById(R.id.rbPN);
		btSave=(Button)findViewById(R.id.bSave);
		InputDataKW=(EditText)findViewById(R.id.etInputKW);
		InputDataPN=(EditText)findViewById(R.id.etInputPN);
		btViewKey=(Button)findViewById(R.id.btKW);
		btViewPhone=(Button)findViewById(R.id.btPN);

		btViewKey.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Bundle databundle = new Bundle();
				databundle.putString("type", "key");
				Intent intentbaru = new Intent(getApplicationContext(),Kategori.class);
				intentbaru.putExtras(databundle);
				startActivityForResult(intentbaru,0);
				
			}
		});
		
		btViewPhone.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Bundle databundle = new Bundle();
				databundle.putString("type", "number");
				Intent intentbaru = new Intent(getApplicationContext(),Kategori.class);
				intentbaru.putExtras(databundle);
				startActivityForResult(intentbaru,0);
				
			}
		});
		
		btSave.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				try {
					
					Boolean cek_data = check_distinct(InputDataKW.getText().toString());
						if (Kategoridipilih.equals("key")){
						
							if(cek_data){
								myDB = openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
								myDB.execSQL("INSERT INTO "+TableName+"(Kategori,Isi) VALUES('"+Kategoridipilih+"','"+InputDataKW.getText().toString().toLowerCase()+"')");
								Toast.makeText(getApplicationContext(), "keyword blocked setting has been saved", Toast.LENGTH_SHORT).show();
								InputDataKW.setText("");
							}else{
								Toast.makeText(getApplicationContext(), "keyword already in blocked list", Toast.LENGTH_SHORT).show();
							}
						} else if(Kategoridipilih.equals("number")){
							if(cek_data){
								myDB = openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
								myDB.execSQL("INSERT INTO "+TableName+"(Kategori,Isi)"+"VALUES('"+Kategoridipilih+"','"+InputDataPN.getText().toString().toLowerCase()+"');");
								Toast.makeText(getApplicationContext(), "phone blocked setting has been saved", Toast.LENGTH_SHORT).show();
								InputDataPN.setText("");
							}else{
								Toast.makeText(getApplicationContext(), "phone already in blocked list", Toast.LENGTH_SHORT).show();								
							}
						}
				}
				catch (Exception e){
					Log.e("Error", "Error",e);
				}
				finally{
					if (myDB!=null)
						myDB.close();
				}
			}
		});
		TypeKeyword.setChecked(true);
		
	}
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch(checkedId){
			case R.id.rbKW:
				Kategoridipilih="key";
				InputDataKW.setVisibility(View.VISIBLE);
				InputDataPN.setVisibility(View.GONE);
			break;
			case R.id.rbPN:
				Kategoridipilih="number";
				InputDataKW.setVisibility(View.GONE);
				InputDataPN.setVisibility(View.VISIBLE);
			break;
		}
		
	}
	
	
	public List<String> getDataFilter(String kategori){
        
        try{
			myDB = this.openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
			myDB.execSQL("CREATE TABLE IF NOT EXISTS "+TableName+"(Kategori VARCHAR, Isi VARCHAR);");
			
			Cursor c =myDB.rawQuery("SELECT * FROM "+TableName+" WHERE Kategori='"+kategori+"'", null);
		
			int Coloumn2= c.getColumnIndex("Isi");
			
			c.moveToFirst();
			if (c!=null){
				do {
					
					String Isi= c.getString(Coloumn2);
					Data = Data+Isi;
				}
				while (c.moveToNext());
			}
			
			
		}
		catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		
        
           return DataFilter;

       }

    public boolean check_distinct(String keyorphone){
    	Boolean hasilcek=true;
    	
    	try{
	    	myDB = openOrCreateDatabase("SMSFiltering", MODE_PRIVATE, null);
	    	Cursor c =myDB.rawQuery("SELECT * FROM "+TableName+" WHERE Isi='"+keyorphone+"'", null);
	    	
	    	c.moveToLast();
	    	//Toast.makeText(getApplicationContext(),""+c.getCount()+"", Toast.LENGTH_LONG).show();
			
	    	if (c.getCount()!=0){
	    		hasilcek= false;
	    	}else{
	    		hasilcek= true;
	    	}
    	}
    	catch (Exception e){
			Log.e("Error", "Error",e);
		}
		finally{
			if (myDB!=null)
				myDB.close();
		}
		return hasilcek;
    	
    }
    
 
// Tombol Menu    
    public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater awesome = getMenuInflater();
		awesome.inflate(R.menu.main_menu, menu); 
		return true;
	}
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menuMessage:
			startActivity(new Intent("project.kp.FILTER"));
			return false;
		case R.id.menuUtama:
			startActivity(new Intent("project.kp.UTAMA"));
			return false;
		
		}
		return false;
	}
    public void onBackItemSelected(){
    	startActivity(new Intent("project.kp.UTAMA"));
    }
    
}
